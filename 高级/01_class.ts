/**
 * TS中的class，不仅提供了class的语法功能，定义的类也作为一种类型存在
 */
class Person {
    name: string,
    age: number,
    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }
}
const p: Person = new Person('张三', 18)

console.log(p.name);
console.log(p.age);

/**
 * class类通过implements关键字实现接口
 * 子类必须显示提供父接口中所有属性
 * 子类可以同时实现多个接口，使用 , 分隔
 */

interface Person{
    name: string,
    say: Function
}
interface Behave {
    eat(): void
}

class Student implements Person, Behave {
    name: '姓名';
    say = () => {
        console.log(`hello!`);
    }
    eat = () => {
        console.log('吃饭');
    }
}
