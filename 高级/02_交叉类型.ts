interface A { a: number };
interface B { b: number };
type AB = A & B;
//使用类型交叉后，类型AB就具有了类型A、B的属性结构
let ab: AB = {
    a: 1,
    b: 2
}
console.log(ab);


