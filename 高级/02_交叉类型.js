;
;
//使用类型交叉后，类型AB就具有了类型A、B的属性结构
var ab = {
    a: 1,
    b: 2
};
console.log(ab);
