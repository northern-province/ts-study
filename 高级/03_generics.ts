/**
 * 创建泛型函数
 *  语法：在函数名称的后面添加 <>（尖括号），尖括号中添加类型变量，比如此处的Type（符合命名规范即可）。
 *  类型变量 Type，是一种特殊类型的变量，它处理类型而不是值。
 *  因为 Type 是类型，因此可以将其作为函数参数和返回值的类型，表示参数和返回值具有相同的类型。
 */
function fn<T>(value: T): T {
    return value
}
console.log(fn<number>(1));

function MyFn<T>(a: T, b: T): T[] {
    return [a, b]
}
console.log(MyFn<number>(1, 2));
/**
 * 为泛型添加约束
 *  通过extends关键字，确保传入的参数类型继承过IAnimal接口，或拥有name属性（参考类型兼容性）
 *  为什么使用泛型约束：类型变量可以代表任意类型，同时也导致实际传入的对象无法访问任何属性，有时就需要为泛型添加约束来收缩类型（缩窄类型取值范围）。
 */
interface IAnimal {
    name: string
}
function eat<T extends IAnimal>(animal: T): void {
    console.log(animal.name);
}

eat('Ac');











