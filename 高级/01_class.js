/**
 * TS中的class，不仅提供了class的语法功能，定义的类也作为一种类型存在
 */
// class Person {
//     name: string,
//     age: number,
//     constructor(name: string, age: number) {
//         this.name = name;
//         this.age = age;
//     }
// }
// const p: Person = new Person('张三', 18)
var Student = /** @class */ (function () {
    function Student() {
        this.say = function () {
            console.log("hello!");
        };
        this.eat = function () {
            console.log('吃饭');
        };
    }
    return Student;
}());
var p = new Student();
p.say();
p.eat();
