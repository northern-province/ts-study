/**
 * js已有的数据类型
 * number、string、boolean、undefined、Function
 */
var Name = '张三';
var age = 18;
var married = false;
var nothing;
var getMobile = function () {
    return '12312312313';
};
/**
 * Object类型：null、数组、对象
 * number、string、boolean、undefined、Function
 */
var wife = null;
var numbers = [1, 2, 3];
var strings = ['1', '2', '3'];
