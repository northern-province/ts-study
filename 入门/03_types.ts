/**
 * js已有的数据类型
 * number、string、boolean、undefined、Function
 */
let Name: string = '张三'
let age: number = 18;
let married: boolean = false;
let nothing: undefined;
let getMobile: Function = function () {
    return '12312312313'
}

/**
 * Object类型：null、数组、对象
 * number、string、boolean、undefined、Function
 */
let wife: null = null;
let numbers: number[] = [1, 2, 3];
let strings: Array<string> = ['1', '2', '3']


