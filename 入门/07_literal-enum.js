/**
 * 字面量使用场景：往往用来表示一组固定的可选值列表（字面量类型+联合类型）
 * 调用此函数传递的参数必须为指定字面量类型中的一个（类似于枚举）
 */
var moveFun = function (direction) {
    console.log(direction);
};
moveFun('down');
/**
 * 枚举：使用enum关键字定义
 */
var direction;
(function (direction) {
    direction[direction["UP"] = 0] = "UP";
    direction[direction["DOWN"] = 1] = "DOWN";
    direction[direction["LEFT"] = 2] = "LEFT";
    direction[direction["RIGHT"] = 3] = "RIGHT";
})(direction || (direction = {}));
/**
 * 枚举常量列表默认值为number，从0开始自增
 */
console.log(direction.UP); // 0
console.log(direction.DOWN); // 1
console.log(direction.LEFT); // 2
console.log(direction.RIGHT); // 3
/**
 * 自定义枚举常量值
 */
var direction2;
(function (direction2) {
    direction2["UP"] = "UP";
    direction2["DOWN"] = "DOWN";
    direction2["LEFT"] = "LEFT";
    direction2["RIGHT"] = "RIGHT";
})(direction2 || (direction2 = {}));
/**
 * 枚举作为参数使用
 */
var moveFun2 = function (direction) {
    console.log(direction);
};
moveFun2(direction2.UP);
/**
 * 将变量obj定义为any类型，所以任意改变obj类型值都不会编译报错
 * 应尽量避免使用any类型，any会使TS失去类型保护的优势
 */
var obj = 1;
obj = '123';
obj = { "name": "zs" };
console.log(obj);
