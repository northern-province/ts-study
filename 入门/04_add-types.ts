/**
 * 联合类型：一个变量可能存在多种数据类型，使用|符号
 * 1、普通变量存在多种类型
 * 2、数据中存在多种数据类型
 */

let a: (string | number) = '字符串'
a = 18

let arrs: (number | string | boolean)[] = [1, '2', true];
let arrs2: Array<(number | string | boolean)> = [1, '2', true];

/**
 * 类型别名：当同一类型（复杂）被多次使用时，可以通过类型别名，简化该类型的使用，使用type关键字
 */
type myArrType = (number | string | boolean)[];
let arr3: myArrType = [1, '2', true];

console.log(arr3);