/**
 * 联合类型：一个变量可能存在多种数据类型，使用|符号
 * 1、普通变量存在多种类型
 * 2、数据中存在多种数据类型
 */
var a = '字符串';
a = 18;
var arrs = [1, '2', true];
var arrs2 = [1, '2', true];
var arr3 = [1, '2', true];
console.log(arr3);
/**
 * 函数参数和返回值类型
 */
// 普通写法
function addFun(num1, num2) {
    return num1 + num2;
}
console.log(addFun(1, 2));
