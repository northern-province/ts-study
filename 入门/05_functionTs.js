/**
 * 函数参数和返回值类型
 */
// 普通写法
function addFun(num1, num2) {
    return num1 + num2;
}
// 箭头函数写法
var squareFun = function (num) {
    return num * num;
};
//箭头函数另类写法
var subFun = function (num1, num2) {
    console.log(num1);
    console.log(num2);
};
//当函数参数可选（参数名后面添加?符号）
var printFun = function (str1, str2) {
    console.log(str1);
    console.log(str2);
};
printFun('1', '2');
printFun('1');
