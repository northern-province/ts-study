/**
 * json对象
 * 直接使用{}描述对象结构
 * 采用属性名: 类型 格式描述属性列表，多个属性使用 ;或, 符号分割
 * 可选属性在属性名后使用? 符号
 */

type personType = { name: string; age: number; married: boolean; emile?: string; say: Function }

let person: personType = {
    name: 'zs',
    age: 20,
    married: true,
    say: (): void => {
        console.log(`hello ${person.name}`);
    }
}

/**
 * interface
 * 直接使用{}描述对象结构
 * 采用属性名: 类型 格式描述属性列表，多个属性使用 ;或, 符号分割
 * 可选属性在属性名后使用? 符号
 */

interface personType1 { name: string; age: number; married: boolean; emile?: string; say: Function }
let person1: personType1 = {
    name: 'zs',
    age: 20,
    married: true,
    say: (): void => {
        console.log(`hello ${person1.name}`);
    }
}

interface studentType extends personType1 { studentNumber: string }

/**
 * interface（接口）和 type（类型别名）的对比：
 *  相同点：都可以给对象指定类型。
 *  不同点：
 *      接口只能为对象指定类型，类型别名可以为任意类型指定别名。
 *      接口可以进行继承，类型别名不能继承。
 */



