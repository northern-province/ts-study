/**
 * 函数参数和返回值类型
 */
// 普通写法
function addFun(num1: number, num2: number): number {
    return num1 + num2
}
// 箭头函数写法
const squareFun = (num: number): number => {
    return num * num
}
//箭头函数另类写法
const subFun = (num1: number, num2: number): void => {
    console.log(num1);
    console.log(num2);
}
//当函数参数可选（参数名后面添加?符号）
const printFun = (str1: string, str2?: string): void => {
    console.log(str1);
    console.log(str2);
}





