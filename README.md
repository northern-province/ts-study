 ## 入门
    ## 1、JS已有数据类型：number、string、boolean、undefined、function、object、symbol（null属于object类型）。
    
    ## 2、TS新增类型：联合类型、自定义类型（类型别名）、接口、元组、字面量类型、枚举、void、any 等。
## 高级
    ## class 类定义
    ## 类型兼容性
    ## 交叉类型
    ## 泛型 和 keyof
    ## 索引签名类型 和 索引查询类型
    ## 映射类型


